import 'package:flutter/material.dart';
import 'package:mobile_rifqi/ListData.dart';
import 'package:mobile_rifqi/TakePhoto.dart';
import 'package:mobile_rifqi/apicovid.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final Size _ukuran = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          children: <Widget>[
            Container(
              height: 100,
              width: 100,
              child: Image.asset("assets/anjgroup.png"),
            ),
            SizedBox(
              height: 40,
            ),
            InkWell(
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => TakePhoto()));
              },
              child: Container(
                width: _ukuran.width / 1.5,
                height: 30,
                color: Colors.yellow[900],
                //highlightColor: Colors.red,
                child: Center(
                  child: Text(
                    "Take a Photo",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            ),
            InkWell(
              child: Container(
                width: _ukuran.width / 1.5,
                height: 30,
                color: Colors.yellow[900],
                //highlightColor: Colors.red,
                child: Center(
                  child: Text(
                    "List Data",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
              onTap: (() {
                Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => ListData()),
                );
              }),
            ),
            SizedBox(
              height: 40,
            ),
            InkWell(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => ApiCovidPage()));
              },
              child: Container(
                width: _ukuran.width / 1.5,
                height: 30,
                color: Colors.yellow[900],
                //highlightColor: Colors.red,
                child: Center(
                  child: Text(
                    "API Covid-19",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
