import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class ApiCovidPage extends StatefulWidget {
  ApiCovidPage({Key key}) : super(key: key);

  @override
  _ApiCovidPageState createState() => _ApiCovidPageState();
}

class _ApiCovidPageState extends State<ApiCovidPage> {
  List data;
  var lastdata, recovered, death, confirmed, getdata;
  @override
  void initState() {
    super.initState();
    getcount();
  }

  @override
  Widget build(BuildContext context) {
    return confirmed.toString() == null
        ? Scaffold(
            appBar: AppBar(
              title: Text("Please Wait ..."),
            ),
          )
        : Scaffold(
            appBar: AppBar(
              title: Text("Back"),
            ),
            backgroundColor: Colors.white,
            // resizeToAvoidBottomPadding: true,
            body: Center(child: _contentApi()),
          );
  }

  Widget _contentApi() {
    final dashtextbold =
        TextStyle(color: Colors.black, fontWeight: FontWeight.bold);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(
            left: 39,
            bottom: 10,
          ),
          child: Row(
            children: <Widget>[
              Container(
                child: Text("Confirmed : "),
              ),
              SizedBox(
                width: 20,
              ),
              Text(
                confirmed.toString() == null
                    ? Text("Please wait..")
                    : confirmed.toString(),
                style: dashtextbold,
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, bottom: 10),
          child: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 20),
                child: Text("Death : "),
              ),
              SizedBox(
                width: 50,
              ),
              Text(
                death.toString() == null
                    ? Text("Please wait..")
                    : death.toString(),
                style: dashtextbold,
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, bottom: 10),
          child: Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 20),
                child: Text("recovered : "),
              ),
              SizedBox(
                width: 23,
              ),
              Text(
                recovered.toString() == null
                    ? Text("Please wait..")
                    : recovered.toString(),
                style: dashtextbold,
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, bottom: 10),
          child: Row(
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(left: 20),
                    child: Text("Last Update : "),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    lastdata.toString() == null
                        ? Text("Please wait..")
                        : lastdata.toString(),
                    style: dashtextbold,
                  )
                ],
              ),
            ],
          ),
        ),
        Text(lastdata.toString()) == null
            ? Text("Load Data..")
            : Text("Data Loaded....")
      ],
    );
  }

  Future<dynamic> getcount() async {
    http.Response response = await http.get(
        "https://services1.arcgis.com/0MSEUqKaxRlEPj5g/arcgis/rest/services/Coronavirus_2019_nCoV_Cases/FeatureServer/1/query?where=UPPER(Country_Region)%20like%20%27%25INDONESIA%25%27&outFields=Last_Update,Recovered,Deaths,Confirmed&returnGeometry=false&outSR=4326&f=json",
        headers: {
          'Accept': 'application/json',
        });
// print("nilai dari tulisan token : $tokenplaylist");
    var content = json.decode(response.body);
    getdata = content['features'][0]['attributes'];
    lastdata = getdata['Last_Update'];
    recovered = getdata['Recovered'];
    death = getdata['Deaths'];
    confirmed = getdata['Confirmed'];
    setState(() {
      print("lastdata : $lastdata");
      print("recovered : $recovered");
      print("death : $death");
      print("Confirmed : $confirmed");
    });
  }
}
